package com.example.fivestomp.service;

import com.example.fivestomp.Dao.AccountJpaRepository;
import com.example.fivestomp.domain.Greeting;
import com.example.fivestomp.model.Account;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
//@RequiredArgsConstructor
@Transactional
public class AccountService {
    private final AccountJpaRepository accountJpaRepository;
    private final SimpMessagingTemplate simpMessagingTemplate;
    public AccountService(AccountJpaRepository accountJpaRepository, SimpMessagingTemplate simpMessagingTemplate) {
        this.accountJpaRepository = accountJpaRepository;
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    public boolean depositAccount(String number, Double amount) throws IOException, InterruptedException {
        Account account = accountJpaRepository.findByNumber(number);

        if(account != null){
            simpMessagingTemplate.convertAndSend("/topic/hello.user",
                    new Greeting("WARNING(DEPOSIT)!  Account number '" + number + "' has been just diposited with ammount " + amount ));
            return this.depositAccount(account, amount);
        }
        return false;
    }
    public boolean depositAccount(Account account, Double amount){
        account.setBalance(account.getBalance() + amount);
        accountJpaRepository.save(account);
        return true;
    }
    public boolean withdrawalMoney(String number, Double amount){
        System.out.println(number);
        System.out.println(amount);
        Account account = accountJpaRepository.findByNumber(number);
        if(account.getBalance() >= amount){
            simpMessagingTemplate.convertAndSend("/topic/hello.user",
                    new Greeting("WARNING(WITHDRAWAL)!  Account number  '" + number + "' has been just withdrawaled with ammount " + amount ));
            return this.withdrawalMoney(account, amount);
        }
        return false;
    }
    public boolean withdrawalMoney(Account account, Double amount){
        if(account.getBalance() >= amount){
            account.setBalance(account.getBalance() - amount);
            accountJpaRepository.save(account);
            return true;
        }
        return false;
    }
    public boolean sendMoney(String numberSender, String numberReceiver, Double amount){
        Account accountSender = accountJpaRepository.findByNumber(numberSender);
        Account accountReceiver = accountJpaRepository.findByNumber(numberReceiver);
        if(!accountSender.getCurrency().equals(accountReceiver.getCurrency())){
            return false;
        }
        if(!this.withdrawalMoney(accountSender, amount)){
            return false;
        };
        if(!this.depositAccount(accountReceiver, amount)){
            this.depositAccount(accountSender, amount);
            return false;
        };
        simpMessagingTemplate.convertAndSend("/topic/hello.user",
        new Greeting("WARNING(MONEY SENT)!  From Account number  '" + numberSender + "' to Account number '" + numberReceiver + "' has been just sent ammount " + amount ));
        return true;
    }
}
