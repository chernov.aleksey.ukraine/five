package com.example.fivestomp.domain;

import lombok.Data;

@Data
public class HelloMessage {
    private String name;
}
